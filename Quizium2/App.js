import React from 'react';
import { StyleSheet, View, Button} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import RegisterScreen from './src/screens/RegisterScreen';
import MainPageScreen from './src/screens/MainPageScreen';
import SignInScreen from './src/screens/SignInScreen';
 
const App = () => {
  return (
    <View style={styles.root}>
      <SignInScreen/>
      
      
      
    </View>
  );
};
 
const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor:'#F9FBFC'
  },
});
export default App;