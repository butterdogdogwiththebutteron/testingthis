
export default [
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Chair",
            },
            {
                id:"1",
                options:"B",
                answer:"sofa",
            },
            {
                id:"2",
                options:"C",
                answer:"Desk",
            },
            {
                id:"3",
                options:"D",
                answer:"Clock",
            },
        ],
        correctAnswerIndex: 1
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Chair",
            },
            {
                id:"2",
                options:"C",
                answer:"Beanbag",
            },
            {
                id:"3",
                options:"D",
                answer:"Clock",
            },
        ],
        correctAnswerIndex: 2
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Bed",
            },
            {
                id:"1",
                options:"B",
                answer:"Chair",
            },
            {
                id:"2",
                options:"C",
                answer:"Desk",
            },
            {
                id:"3",
                options:"D",
                answer:"Clock",
            },
        ],
        correctAnswerIndex: 0
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Bed",
            },
            {
                id:"2",
                options:"C",
                answer:"Desk",
            },
            {
                id:"3",
                options:"D",
                answer:"Bookshelf",
            },
        ],
        correctAnswerIndex: 3
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Chair",
            },
            {
                id:"2",
                options:"C",
                answer:"Bunkbed",
            },
            {
                id:"3",
                options:"D",
                answer:"Clock",
            },
        ],
        correctAnswerIndex: 2
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Stool",
            },
            {
                id:"2",
                options:"C",
                answer:"Chair",
            },
            {
                id:"3",
                options:"D",
                answer:"Clock",
            },
        ],
        correctAnswerIndex: 2
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Chair",
            },
            {
                id:"2",
                options:"C",
                answer:"Clock",
            },
            {
                id:"3",
                options:"D",
                answer:"Wardrobe",
            },
        ],
        correctAnswerIndex: 2
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Coat Rack",
            },
            {
                id:"2",
                options:"C",
                answer:"Desk",
            },
            {
                id:"3",
                options:"D",
                answer:"Coffee Table",
            },
        ],
        correctAnswerIndex: 1
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Coffee Table",
            },
            {
                id:"2",
                options:"C",
                answer:"Desk",
            },
            {
                id:"3",
                options:"D",
                answer:"Clock",
            },
        ],
        correctAnswerIndex: 1
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Desk",
            },
            {
                id:"1",
                options:"B",
                answer:"Chair",
            },
            {
                id:"2",
                options:"C",
                answer:"Mattress",
            },
            {
                id:"3",
                options:"D",
                answer:"Clock",
            },
        ],
        correctAnswerIndex: 0
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Chair",
            },
            {
                id:"2",
                options:"C",
                answer:"Desk",
            },
            {
                id:"3",
                options:"D",
                answer:"Dining Table",
            },
        ],
        correctAnswerIndex: 3
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Chair",
            },
            {
                id:"2",
                options:"C",
                answer:"Bunk Bed",
            },
            {
                id:"3",
                options:"D",
                answer:"Dresser",
            },
        ],
        correctAnswerIndex: 3
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Tv Stand",
            },
            {
                id:"1",
                options:"B",
                answer:"Chair",
            },
            {
                id:"2",
                options:"C",
                answer:"Desk",
            },
            {
                id:"3",
                options:"D",
                answer:"Lamp",
            },
        ],
        correctAnswerIndex: 3
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Chair",
            },
            {
                id:"2",
                options:"C",
                answer:"Mattress",
            },
            {
                id:"3",
                options:"D",
                answer:"Stool",
            },
        ],
        correctAnswerIndex: 2
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Night Stand",
            },
            {
                id:"2",
                options:"C",
                answer:"Coat Rack",
            },
            {
                id:"3",
                options:"D",
                answer:"Clock",
            },
        ],
        correctAnswerIndex: 1
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Chair",
            },
            {
                id:"2",
                options:"C",
                answer:"Desk",
            },
            {
                id:"3",
                options:"D",
                answer:"Clock",
            },
        ],
        correctAnswerIndex: 0
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Stool",
            },
            {
                id:"1",
                options:"B",
                answer:"Mattress",
            },
            {
                id:"2",
                options:"C",
                answer:"Desk",
            },
            {
                id:"3",
                options:"D",
                answer:"Clock",
            },
        ],
        correctAnswerIndex: 0
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Chair",
            },
            {
                id:"2",
                options:"C",
                answer:"Desk",
            },
            {
                id:"3",
                options:"D",
                answer:"Storage Cabinet",
            },
        ],
        correctAnswerIndex: 3
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"sofa",
            },
            {
                id:"1",
                options:"B",
                answer:"Tv Stand",
            },
            {
                id:"2",
                options:"C",
                answer:"Storage cabinet",
            },
            {
                id:"3",
                options:"D",
                answer:"Clock",
            },
        ],
        correctAnswerIndex: 1
    },
    {
        question: "What is this furniture?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Wardrobe",
            },
            {
                id:"1",
                options:"B",
                answer:"Lamp",
            },
            {
                id:"2",
                options:"C",
                answer:"Sofa",
            },
            {
                id:"3",
                options:"D",
                answer:"Bed",
            },
        ],
        correctAnswerIndex: 0
    }
]