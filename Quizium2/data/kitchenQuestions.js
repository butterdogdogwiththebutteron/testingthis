
export default [
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Fork",
            },
            {
                id:"1",
                options:"B",
                answer:"Blender",
            },
            {
                id:"2",
                options:"C",
                answer:"Kettle",
            },
            {
                id:"3",
                options:"D",
                answer:" Mixer",
            },
        ],
        correctAnswerIndex: 1
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Frying Pan",
            },
            {
                id:"1",
                options:"B",
                answer:"Fork",
            },
            {
                id:"2",
                options:"C",
                answer:"Butter Knife",
            },
            {
                id:"3",
                options:"D",
                answer:" Mixer",
            },
        ],
        correctAnswerIndex: 2
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Chef Knife",
            },
            {
                id:"1",
                options:"B",
                answer:"Spoon",
            },
            {
                id:"2",
                options:"C",
                answer:"Kettle",
            },
            {
                id:"3",
                options:"D",
                answer:" Mixer",
            },
        ],
        correctAnswerIndex: 0
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Frying Pan",
            },
            {
                id:"1",
                options:"B",
                answer:"Mixer",
            },
            {
                id:"2",
                options:"C",
                answer:"Kettle",
            },
            {
                id:"3",
                options:"D",
                answer:" Oven",
            },
        ],
        correctAnswerIndex: 3
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Frying Pan",
            },
            {
                id:"1",
                options:"B",
                answer:"Fork",
            },
            {
                id:"2",
                options:"C",
                answer:"Cooking Pot",
            },
            {
                id:"3",
                options:"D",
                answer:" Mixer",
            },
        ],
        correctAnswerIndex: 2
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Frying Pan",
            },
            {
                id:"1",
                options:"B",
                answer:"Pomegranet",
            },
            {
                id:"2",
                options:"C",
                answer:"Cutting Board",
            },
            {
                id:"3",
                options:"D",
                answer:" Mixer",
            },
        ],
        correctAnswerIndex: 2
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Frying Pan",
            },
            {
                id:"1",
                options:"B",
                answer:"Microwave",
            },
            {
                id:"2",
                options:"C",
                answer:"Fork",
            },
            {
                id:"3",
                options:"D",
                answer:" Mixer",
            },
        ],
        correctAnswerIndex: 2
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Grater",
            },
            {
                id:"1",
                options:"B",
                answer:"Frying Pan",
            },
            {
                id:"2",
                options:"C",
                answer:"Kettle",
            },
            {
                id:"3",
                options:"D",
                answer:"Strawberry",
            },
        ],
        correctAnswerIndex: 1
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Frying Pan",
            },
            {
                id:"1",
                options:"B",
                answer:"Grater",
            },
            {
                id:"2",
                options:"C",
                answer:"Kettle",
            },
            {
                id:"3",
                options:"D",
                answer:" Mixer",
            },
        ],
        correctAnswerIndex: 1
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Ice Cream Scooper",
            },
            {
                id:"1",
                options:"B",
                answer:"Fork",
            },
            {
                id:"2",
                options:"C",
                answer:"Kettle",
            },
            {
                id:"3",
                options:"D",
                answer:" Mixer",
            },
        ],
        correctAnswerIndex: 0
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Frying Pan",
            },
            {
                id:"1",
                options:"B",
                answer:"Fork",
            },
            {
                id:"2",
                options:"C",
                answer:"Ice Cream Scooper",
            },
            {
                id:"3",
                options:"D",
                answer:"Kettle",
            },
        ],
        correctAnswerIndex: 3
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Frying Pan",
            },
            {
                id:"1",
                options:"B",
                answer:"Fork",
            },
            {
                id:"2",
                options:"C",
                answer:"Rice Cooker",
            },
            {
                id:"3",
                options:"D",
                answer:"Microwave",
            },
        ],
        correctAnswerIndex: 3
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Wok",
            },
            {
                id:"1",
                options:"B",
                answer:"Fork",
            },
            {
                id:"2",
                options:"C",
                answer:"Kettle",
            },
            {
                id:"3",
                options:"D",
                answer:"Mixer",
            },
        ],
        correctAnswerIndex: 3
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Frying Pan",
            },
            {
                id:"1",
                options:"B",
                answer:"Fork",
            },
            {
                id:"2",
                options:"C",
                answer:"Peeler",
            },
            {
                id:"3",
                options:"D",
                answer:"Whisk",
            },
        ],
        correctAnswerIndex: 2
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Frying Pan",
            },
            {
                id:"1",
                options:"B",
                answer:"Refridgerator",
            },
            {
                id:"2",
                options:"C",
                answer:"Whisk",
            },
            {
                id:"3",
                options:"D",
                answer:" Mixer",
            },
        ],
        correctAnswerIndex: 1
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Rice Cooker",
            },
            {
                id:"1",
                options:"B",
                answer:"Fork",
            },
            {
                id:"2",
                options:"C",
                answer:"Kettle",
            },
            {
                id:"3",
                options:"D",
                answer:"Wok",
            },
        ],
        correctAnswerIndex: 0
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Spoon",
            },
            {
                id:"1",
                options:"B",
                answer:"Toaster",
            },
            {
                id:"2",
                options:"C",
                answer:"Whisk",
            },
            {
                id:"3",
                options:"D",
                answer:" Mixer",
            },
        ],
        correctAnswerIndex: 0
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Frying Pan",
            },
            {
                id:"1",
                options:"B",
                answer:"Fork",
            },
            {
                id:"2",
                options:"C",
                answer:"Kettle",
            },
            {
                id:"3",
                options:"D",
                answer:"Toaster",
            },
        ],
        correctAnswerIndex: 3
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Frying Pan",
            },
            {
                id:"1",
                options:"B",
                answer:"Whisk",
            },
            {
                id:"2",
                options:"C",
                answer:"Wok",
            },
            {
                id:"3",
                options:"D",
                answer:" Mixer",
            },
        ],
        correctAnswerIndex: 1
    },
    {
        question: "What is this Kitchen Item?",
        options:[
            {
                id:"0",
                options:"A",
                answer:"Wok",
            },
            {
                id:"1",
                options:"B",
                answer:"Kettle",
            },
            {
                id:"2",
                options:"C",
                answer:"Microwave",
            },
            {
                id:"3",
                options:"D",
                answer:"Peeler",
            },
        ],
        correctAnswerIndex: 0
    }
]