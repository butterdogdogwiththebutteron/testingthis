import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
 export const firebaseConfig = {
  apiKey: "AIzaSyA5dSwlJciLsI86a2ARbjNe6HKCAdnAHNw",
  authDomain: "autismproject-693cd.firebaseapp.com",
  databaseURL: "https://autismproject-693cd-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "autismproject-693cd",
  storageBucket: "autismproject-693cd.appspot.com",
  messagingSenderId: "64343013196",
  appId: "1:64343013196:web:550c560ae60b872d9e81e9",
  measurementId: "G-682SSQMW64"
};

export const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);