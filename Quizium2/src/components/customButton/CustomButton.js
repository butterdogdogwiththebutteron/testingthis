import { View, Text, StyleSheet,Pressable} from 'react-native'
import React from 'react'

const CustomButton = ({onPress,text,type="PRIMARY",bgColour,fgColour,size=15}) => {
  return (
    <Pressable onPress={onPress} style={[styles.container, styles[`container_${type}`],bgColour ? {backgroundColor:bgColour}:{},size ? {padding:size}:{}]} >
      <Text style={[styles.text, styles[`text_${type}`],fgColour ? {color:fgColour} : {}]}>{text}</Text>
    </Pressable>
  )
}

const styles = StyleSheet.create({
  container:{
    
    
    width:'100%',
    marginVertical: 5,
    
    alignItems:'center',
    borderRadius:5,
  },
  container_PRIMARY:{
    backgroundColor:'#3B71F3',
  },
  container_TERTIARY:{

  },
  text:{

    fontWeight:'bold',
    color:'white'
  },
  text_TERTIARY:{
    color:'gray'
  }
}) 

export default CustomButton