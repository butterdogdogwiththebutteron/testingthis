import { View, Text,StyleSheet } from 'react-native'
import React from 'react'
import CustomButton from '../../components/customButton';
import { useNavigation} from '@react-navigation/native';
import { createNativeStackNavigator, NativeStackView} from '@react-navigation/native-stack';

function MainPageScreen () {
    const navigation = useNavigation();

    const onCategoriesPressed = () => {

        console.warn("Categories")
        navigation.navigate('Categories');
    }   
    const onPointsPressed = () => {
        console.warn("Points")
        navigation.navigate('Points');
    }
    const onNewQuizPressed = () => {
        console.warn("new quiz")
        navigation.navigate('CreateQuiz');
    }



  return (
    <View>
        <Text style={style.title}> Main Page</Text>  
         <CustomButton size={30} bgColour='#FFC600' text="Categories" onPress={onCategoriesPressed} />
         <CustomButton size={30} bgColour='#009A17' text="Points" onPress={onPointsPressed}/>
         <CustomButton size={30} bgColour='#9050F6' text="Create Quiz" onPress={onNewQuizPressed}/>
    </View>
  )
}

const style=StyleSheet.create({
    
    title:{
        alignItems:'center',
        padding:120,
        fontWeight:'bold',
        fontSize:25
    },
    
})

;
const Stack = createNativeStackNavigator();

export default MainPageScreen