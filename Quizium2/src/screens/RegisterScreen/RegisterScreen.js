import React from 'react';
import { Text, View, Image, StyleSheet, useWindowDimensions,ScrollView, Alert} from 'react-native';
import Logo from '../../../assets/quizLogo.png';
import CustomInput from '../../components/CustomInput';
import CustomButton from '../../components/customButton';
import { getAuth, createUserWithEmailAndPassword } from 'firebase/auth';
import { initializeApp } from 'firebase/app';
import { firebaseConfig } from '../../../Firebase/firebaseConfig';
import { createNativeStackNavigator, NativeStackView} from '@react-navigation/native-stack';
import { useNavigation} from '@react-navigation/native';


const RegisterPage=()=>{
    const [email, setEmail] = React.useState('')
    const [password, setPassword] = React.useState('')
    const app = initializeApp(firebaseConfig);
    const auth = getAuth(app);

    const navigation = useNavigation();
    
    
    const handleCreateAccount = () => {
        createUserWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            console.log('Account created!')
            const user = userCredential.user;
            console.log(user)
            navigation.navigate('Login');
            
        })
        .catch(error => {
            console.log(error)
            Alert.alert(error.message)
        })
    }

    return(
        <ScrollView showsHorizontalScrollIndicator={false}>
        <View style={style.root}>
          <Text style={style.title}>Register</Text>  


        <CustomInput placeholder="Email" value={email} setValue={setEmail}/>
         <CustomInput placeholder="Password" value={password} setValue={setPassword} secureTextEntry/>  
         
         <CustomButton text="Register" onPress={handleCreateAccount}/>
        
        
        </View>
        </ScrollView>
    );
};
const style=StyleSheet.create({
    root: {
        alignItems: 'center',
        padding: 20,
    },
    title:{
        alignItems:'center',
        padding:30,
        fontWeight:'bold',
        fontSize:25
    }

});
 
const Stack = createNativeStackNavigator();

export default RegisterPage
